package com.epam.chat.repository.specification.impl.user;

import com.epam.chat.model.entity.User;
import com.epam.chat.model.entity.User_;
import com.epam.chat.repository.specification.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class FindUserByLoginSpecification implements Specification<User> {

    private String login;

    public FindUserByLoginSpecification(String login) {
        this.login = login;
    }

    @Override
    public CriteriaQuery<User> toQuery(CriteriaQuery<User> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Root<User> root = criteriaQuery.from(User.class);
        return criteriaQuery.where(criteriaBuilder.equal(root.get(User_.LOGIN), login));
    }
}
