package com.epam.chat.repository.specification;

import com.epam.chat.model.entity.BaseEntity;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public interface Specification<T extends BaseEntity> {

    CriteriaQuery<T> toQuery(CriteriaQuery<T> criteriaQuery, CriteriaBuilder criteriaBuilder);
}
