package com.epam.chat.repository.specification.impl.chat;

import com.epam.chat.model.entity.Chat;
import com.epam.chat.model.entity.Chat_;
import com.epam.chat.repository.specification.Specification;
import lombok.AllArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class FindChatByIdSpecification implements Specification<Chat> {

    private Long id;

    @Override
    public CriteriaQuery<Chat> toQuery(CriteriaQuery<Chat> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Root<Chat> root = criteriaQuery.from(Chat.class);
        return criteriaQuery.where(criteriaBuilder.equal(root.get(Chat_.ID), id));
    }
}
