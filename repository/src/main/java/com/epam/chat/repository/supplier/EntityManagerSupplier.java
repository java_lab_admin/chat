package com.epam.chat.repository.supplier;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.function.Supplier;

public class EntityManagerSupplier implements Supplier<EntityManager> {

    private final EntityManager entityManager;

    @Inject
    public EntityManagerSupplier(EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public EntityManager get() {
        return entityManager;
    }
}
