package com.epam.chat.repository.specification.impl.message;

import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.Message_;
import com.epam.chat.model.entity.User_;
import com.epam.chat.repository.specification.Specification;
import lombok.AllArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class FindMessageByUserLoginSpecification implements Specification<Message> {
    private String login;

    @Override
    public CriteriaQuery<Message> toQuery(CriteriaQuery<Message> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Root<Message> root = criteriaQuery.from(Message.class);
        return criteriaQuery.where(criteriaBuilder.equal(root.get(Message_.USER).get(User_.LOGIN), login));
    }
}
