package com.epam.chat.repository.impl;

import com.epam.chat.model.entity.BaseEntity;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.Specification;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class RepositoryImpl<T extends BaseEntity> implements Repository<T> {

    private final Class<T> typeClass;

    private EntityManager entityManager;

    public RepositoryImpl(EntityManager entityManager, Class<T> typeClass) {
        this.entityManager = entityManager;
        this.typeClass = typeClass;
    }

    @Override
    public T save(T item) {
        return entityManager.merge(item);
    }

    @Override
    public List<T> save(List<T> items) {
        List<T> resultList = new ArrayList<>();
        for (T item : items) {
            resultList.add(entityManager.merge(item));
        }
        return resultList;
    }

    @Override
    public void remove(T item) {
        entityManager.remove(item);
    }

    @Override
    public void remove(List<T> items) {
        for (T item : items) {
            entityManager.remove(item);
        }
    }

    @Override
    public void removeById(Long id) {
        T item = entityManager.find(typeClass, id);
        entityManager.remove(item);
    }

    @Override
    public List<T> findBy(Specification<T> specification) {
        CriteriaQuery<T> query = buildQuery(specification);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public List<T> findByWithLimit(Specification<T> specification, Integer limit, Integer offset) {
        CriteriaQuery<T> query = buildQuery(specification);
        return entityManager.createQuery(query)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
    }

    @Override
    public Optional<T> findSingleBy(Specification<T> specification) {
        List<T> items = findBy(specification);
        return items.isEmpty() ? Optional.empty() : Optional.of(items.get(0));
    }

    @Override
    public Long getFieldsCount() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        criteriaQuery.select(criteriaBuilder.count(criteriaQuery.from(typeClass)));
        return entityManager
                .createQuery(criteriaQuery)
                .getSingleResult();
    }

    @Override
    public void transactionManage(Consumer<EntityTransaction> consumer) {
        consumer.accept(entityManager.getTransaction());
    }

    private CriteriaQuery<T> buildQuery(Specification<T> specification) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(typeClass);

        return specification.toQuery(query, builder);
    }
}
