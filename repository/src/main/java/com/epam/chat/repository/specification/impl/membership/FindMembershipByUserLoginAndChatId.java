package com.epam.chat.repository.specification.impl.membership;

import com.epam.chat.model.entity.Chat_;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Membership_;
import com.epam.chat.model.entity.User_;
import com.epam.chat.repository.specification.Specification;
import lombok.AllArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class FindMembershipByUserLoginAndChatId implements Specification<Membership> {

    private String login;

    private Long chatId;

    @Override
    public CriteriaQuery<Membership> toQuery(CriteriaQuery<Membership> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Root<Membership> root = criteriaQuery.from(Membership.class);
        return criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(root.get(Membership_.USER).get(User_.LOGIN), login)),
                criteriaBuilder.equal(root.get(Membership_.CHAT).get(Chat_.ID), chatId));
    }
}
