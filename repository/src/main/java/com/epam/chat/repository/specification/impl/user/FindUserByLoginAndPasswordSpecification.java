package com.epam.chat.repository.specification.impl.user;

import com.epam.chat.model.entity.User;
import com.epam.chat.model.entity.User_;
import com.epam.chat.repository.specification.Specification;
import lombok.AllArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class FindUserByLoginAndPasswordSpecification implements Specification<User> {

    private String login;
    private String password;

    @Override
    public CriteriaQuery<User> toQuery(CriteriaQuery<User> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Root<User> root = criteriaQuery.from(User.class);
        return criteriaQuery.where(criteriaBuilder.equal(root.get(User_.LOGIN), login), criteriaBuilder.equal(root.get(User_.PASSWORD), password));
    }
}
