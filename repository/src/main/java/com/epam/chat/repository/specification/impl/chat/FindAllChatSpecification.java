package com.epam.chat.repository.specification.impl.chat;

import com.epam.chat.model.entity.Chat;
import com.epam.chat.repository.specification.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class FindAllChatSpecification implements Specification<Chat> {
    @Override
    public CriteriaQuery<Chat> toQuery(CriteriaQuery<Chat> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Root<Chat> root = criteriaQuery.from(Chat.class);
        return criteriaQuery.select(root);
    }
}
