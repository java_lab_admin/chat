package com.epam.chat.repository.specification.impl.message;

import com.epam.chat.model.entity.Message;
import com.epam.chat.repository.specification.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class FindAllMessageSpecification implements Specification<Message> {

    @Override
    public CriteriaQuery<Message> toQuery(CriteriaQuery<Message> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Root<Message> root = criteriaQuery.from(Message.class);
        return criteriaQuery.select(root);
}
}
