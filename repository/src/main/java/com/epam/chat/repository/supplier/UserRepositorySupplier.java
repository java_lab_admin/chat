package com.epam.chat.repository.supplier;

import com.epam.chat.model.entity.User;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.impl.RepositoryImpl;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.function.Supplier;

public class UserRepositorySupplier implements Supplier<Repository<User>> {

    private final EntityManager entityManager;

    @Inject
    public UserRepositorySupplier(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Repository<User> get() {
        return new RepositoryImpl<>(entityManager, User.class);
    }
}
