package com.epam.chat.repository.specification.impl.membership;

import com.epam.chat.model.entity.Membership;
import com.epam.chat.repository.specification.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class FindAllMembershipSpecification implements Specification<Membership> {
    @Override
    public CriteriaQuery<Membership> toQuery(CriteriaQuery<Membership> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Root<Membership> root = criteriaQuery.from(Membership.class);
        return criteriaQuery.select(root);
    }
}
