package com.epam.chat.service.impl;

import com.epam.chat.model.dto.MessageUpdateDto;
import com.epam.chat.model.dto.UserDto;
import com.epam.chat.model.dto.response.ChatDto;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.User;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.impl.message.FindAllMessageSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByIdSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.NotFoundException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceImplTest {

/*    @Mock
    private Repository<Message> messageRepository;

    @InjectMocks
    private MessageServiceImpl messageService;

    private Long messageId = 1L;
    private String userLogin = "login";
    private String messageText = "message text";
    private LocalDateTime dateCreation = LocalDateTime.now();
    private Message message;
    private User user;
    private MessageUpdateDto messageDto;
    private UserDto userDto;

    @Before
    public void initData() {
        message = new Message();
        user = new User();
        user.setLogin(userLogin);
        message.setText(messageText);
        message.setCreation(dateCreation);
        message.setUser(user);

        userDto = new UserDto();
        userDto.setLogin(userLogin);
        messageDto = new MessageUpdateDto();
        messageDto.setUser(userDto);
        messageDto.setText(messageText);
        messageDto.setCreation(dateCreation);

        ChatDto chatDto = new ChatDto();
        chatDto.setName("Chat");
        messageDto.setChat(chatDto);
    }

    @Test
    public void shouldReturnSpecificMessageByItsId() {
        message.setId(messageId);
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.of(message));

        MessageUpdateDto returnedMessageDto = messageService.findById(messageId);
        Assert.assertEquals(messageId, returnedMessageDto.getId());
        Assert.assertEquals(messageText, returnedMessageDto.getText());
        Assert.assertEquals(userLogin, returnedMessageDto.getUser().getLogin());
    }

    @Test
    public void shouldReturnListOfAllMessages() {
        message.setId(messageId);
        when(messageRepository.findBy(any(FindAllMessageSpecification.class))).thenReturn(Arrays.asList(message));

        List<MessageUpdateDto> messages = messageService.findAll();
        Assert.assertEquals(1, messages.size());

        MessageUpdateDto returnedDto = messages.get(0);

        Assert.assertEquals(messageId, returnedDto.getId());
        Assert.assertEquals(messageText, returnedDto.getText());
        Assert.assertEquals(dateCreation, returnedDto.getCreation());
        Assert.assertEquals(userLogin, returnedDto.getUser().getLogin());
    }

    @Test
    public void shouldReturnSavedMessageWhenInvokeSaveOfMessageServiceImpl() {
        when(messageRepository.save(any(Message.class))).thenReturn(message);
        MessageUpdateDto savedMessageDto = messageService.save(messageDto);
        Assert.assertNull(savedMessageDto.getId());
        Assert.assertEquals(messageText, savedMessageDto.getText());
        Assert.assertEquals(dateCreation, savedMessageDto.getCreation());
        Assert.assertEquals(userLogin, savedMessageDto.getUser().getLogin());
    }

    @Test
    public void shouldReturnUpdatedMessageWhenInvokeUpdateOfMessageServiceImpl() {
        message.setId(1L);
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.of(message));
        when(messageRepository.save(any(Message.class))).thenReturn(message);

        MessageUpdateDto updatedMessage = messageService.update(messageDto, 1L);

        Assert.assertEquals(messageId, updatedMessage.getId());
        Assert.assertEquals(messageText, updatedMessage.getText());
        Assert.assertEquals(dateCreation, updatedMessage.getCreation());
        Assert.assertEquals(userLogin, updatedMessage.getUser().getLogin());
    }

    @Test
    public void repositoryShouldInvokeDeletingWhenInvokeDeleteOfMessageServiceImpl() {
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.of(message));

        messageService.deleteById(messageId);

        verify(messageRepository, times(1)).removeById(messageId);
    }

    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenCantFindMessageById() throws NotFoundException {
        long missingId = 1L;
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.empty());
        messageService.findById(missingId);
    }

    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenUpdateMissingMessage() throws NotFoundException {
        long missingId = 1L;
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.empty());
        messageService.update(messageDto,missingId);
    }

    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenDeleteMissingMessage() throws NotFoundException {
        long missingId = 1L;
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.empty());
        messageService.deleteById(missingId);
    }*/
}
