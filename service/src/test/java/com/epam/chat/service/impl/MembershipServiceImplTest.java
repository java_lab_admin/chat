package com.epam.chat.service.impl;

import com.epam.chat.model.dto.ChatDtoEmbedded;
import com.epam.chat.model.dto.MembershipCreateDto;
import com.epam.chat.model.dto.UserLoginDtoEmbedded;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.model.entity.Chat;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.User;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.impl.RepositoryImpl;
import com.epam.chat.repository.specification.Specification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MembershipServiceImplTest {
    @Mock
    private Repository<Chat> chatRepository;

    @Mock
    private Repository<User> userRepository;

    @Spy
    private Repository<Membership> membershipRepository;

    private EntityManager entityManager;

    @InjectMocks
    private MembershipServiceImpl membershipService;

    private MembershipCreateDto testMembershipDto;
    private Membership testMembership;

    private ChatDtoEmbedded testChatDto;
    private Chat testChat;

    private UserLoginDtoEmbedded testUserDto;
    private User testUser;

    {
        MockitoAnnotations.initMocks(this);
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("test");
        entityManager = entityManagerFactory.createEntityManager();
        membershipRepository = new RepositoryImpl<>(entityManager, Membership.class);

        testUserDto = new UserLoginDtoEmbedded();
        testUserDto.setLogin("testUserDto");

        testUser = new User();
        testUser.setLogin("testUser");

        testChatDto = new ChatDtoEmbedded();
        testChatDto.setId(1L);

        testChat = new Chat();
        testChat.setId(1L);
        testChat.setName("testChat");

        testMembershipDto = new MembershipCreateDto();
        testMembershipDto.setUser(testUserDto);

        testMembership = new Membership();
        testMembership.setUser(testUser);
        testMembership.setChat(testChat);
    }



    @Test(expected = NotFoundException.class)
    public void shouldThrowExceptionWhenAddMembershipWithNotExistsUser() {
        when(userRepository.findSingleBy(any(Specification.class))).thenReturn(Optional.empty());

//        membershipService.save(testMembershipDto);
    }

    @Test
    public void shouldRemoveMembershipByChatIdAndLoginId() {
        doReturn(Optional.of(testMembership)).when(membershipRepository).findSingleBy(any(Specification.class));
        membershipService.removeByChatIdAndUserLogin(1L, "login");
    }

    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenRemoveMembershipWithNotExistsUserAndChat() {
        doReturn(Optional.empty()).when(membershipRepository).findSingleBy(any(Specification.class));
        membershipService.removeByChatIdAndUserLogin(1L, "login");
    }


    @Test
    public void shouldThrowNotFoundExceptionWhenFindMembershipWithNotExistsUser() {
        List<MembershipDto> result = membershipService.findByUserLogin(testUserDto.getLogin());

        Assert.assertEquals(0, result.size());
    }
}
