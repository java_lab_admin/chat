package com.epam.chat.service;

import com.epam.chat.model.dto.UserDto;
import com.epam.chat.model.dto.UserLoginDtoEmbedded;
import com.epam.chat.model.dto.UserPasswordDtoEmbedded;

public interface UserService {

    UserLoginDtoEmbedded findByLogin(String login);

    UserLoginDtoEmbedded save(UserDto user);

    UserLoginDtoEmbedded updatePassword(UserPasswordDtoEmbedded userDto, String login);

    void delete(String login);
}
