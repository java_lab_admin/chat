package com.epam.chat.service;

import com.epam.chat.model.entity.User;

import java.util.Optional;

public interface AuthService {
    Optional<User> findByLoginAndPassword(String login, String password);
}
