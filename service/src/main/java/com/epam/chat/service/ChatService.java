package com.epam.chat.service;

import com.epam.chat.model.dto.ChatCreateOrUpdateDto;
import com.epam.chat.model.dto.response.ChatDto;

import java.util.List;

public interface ChatService {

    List<ChatDto> findAll();

    ChatDto findById(Long id);

    ChatDto save(ChatCreateOrUpdateDto chatDto, String login);

    ChatDto update(Long id, ChatCreateOrUpdateDto chatDto);

    void removeById(Long id);
}
