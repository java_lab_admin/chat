package com.epam.chat.service.impl;

import com.epam.chat.model.entity.User;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.impl.user.FindUserByLoginAndPasswordSpecification;
import com.epam.chat.service.AuthService;

import javax.inject.Inject;
import java.util.Optional;

public class AuthServiceImpl implements AuthService {

    private Repository<User> userRepository;

    @Inject
    public AuthServiceImpl(Repository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findByLoginAndPassword(String login, String password) {
        return userRepository.findSingleBy(new FindUserByLoginAndPasswordSpecification(login, password));
    }
}
