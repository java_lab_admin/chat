package com.epam.chat.service.impl;

import com.epam.chat.model.dto.ChatDtoEmbedded;
import com.epam.chat.model.dto.MessageCreateDto;
import com.epam.chat.model.dto.MessageUpdateDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.User;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByUserLoginAndChatId;
import com.epam.chat.repository.specification.impl.message.FindAllMessageSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByChatIdSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByIdSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByUserLoginSpecification;
import com.epam.chat.service.MessageService;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.persistence.EntityTransaction;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MessageServiceImpl implements MessageService {

    private static final String MESSAGE_NOT_FOUND = "Message is not found : ";
    private static final String FORBIDDEN_EXCEPTION = "Only chat members can add messages";
    private static final String MESSAGE_UPDATE_FORBIDDEN_EXCEPTION = "Only message author can update message";
    private static final String MESSAGE_DELETE_FORBIDDEN_EXCEPTION = "Only message author or chat admin can delete message";
    private static final String MESSAGE_WRONG_PAGINATION_PARAMETERS = "Limit and/or offset must be greater than zero";


    private ModelMapper modelMapper = new ModelMapper();

    private Repository<Message> messageRepository;

    private Repository<Membership> membershipRepository;

    @Inject
    public MessageServiceImpl(Repository<Message> messageRepository, Repository<Membership> membershipRepository) {
        this.messageRepository = messageRepository;
        this.membershipRepository = membershipRepository;
    }

    public MessageDto findById(Long id) {
        Message message = messageRepository.findSingleBy(new FindMessageByIdSpecification(id))
                .orElseThrow(() -> new NotFoundException(MESSAGE_NOT_FOUND + id));
        return modelMapper.map(message, MessageDto.class);
    }

    public List<MessageDto> findAll() {
        List<Message> messages = messageRepository.findBy(new FindAllMessageSpecification());
        return messages.stream().map(message -> modelMapper.map(message, MessageDto.class)).collect(Collectors.toList());
    }

    public MessageDto save(MessageCreateDto messageDto, String userLogin) {
        ChatDtoEmbedded chatDto = messageDto.getChat();
        membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(userLogin, chatDto.getId()))
                .orElseThrow(() -> new ForbiddenException(FORBIDDEN_EXCEPTION));

        Message messageToSave = modelMapper.map(messageDto, Message.class);
        messageToSave.setCreation(LocalDateTime.now());
        User user = new User();
        user.setLogin(userLogin);
        messageToSave.setUser(user);

        messageRepository.transactionManage(EntityTransaction::begin);
        Message message = messageRepository.save(messageToSave);
        messageRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(message, MessageDto.class);
    }

    public MessageDto update(Long id, MessageUpdateDto messageDto, String userLogin) {
        Message message = messageRepository.findSingleBy(new FindMessageByIdSpecification(id))
                .orElseThrow(() -> new NotFoundException(MESSAGE_NOT_FOUND + id));
        if(!isCurrentUserMessageAuthor(message, userLogin)){
            throw new ForbiddenException(MESSAGE_UPDATE_FORBIDDEN_EXCEPTION);
        }
        message.setText(messageDto.getText());

        messageRepository.transactionManage(EntityTransaction::begin);
        messageRepository.save(message);
        messageRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(message, MessageDto.class);
    }

    public void deleteById(Long id, String userLogin) {
        Message message = messageRepository.findSingleBy(new FindMessageByIdSpecification(id))
                .orElseThrow(() -> new NotFoundException(MESSAGE_NOT_FOUND + id));
        if(!isCurrentUserChatAdmin(id, userLogin) && !isCurrentUserMessageAuthor(message, userLogin)){
            throw new ForbiddenException(MESSAGE_DELETE_FORBIDDEN_EXCEPTION);
        }
        messageRepository.transactionManage(EntityTransaction::begin);
        messageRepository.removeById(id);
        messageRepository.transactionManage(EntityTransaction::commit);
    }

    @Override
    public List<MessageDto> findMessagesByUserLogin(String login) {
        return messageRepository.findBy(new FindMessageByUserLoginSpecification(login))
                .stream()
                .map(message -> modelMapper.map(message, MessageDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<MessageDto> findMessagesByUserLoginWithPagination(String login, Integer limit, Integer offset) {
        if (limit < 0 || offset < 0) {
            throw new BadRequestException(MESSAGE_WRONG_PAGINATION_PARAMETERS);
        }
        return messageRepository.findByWithLimit(new FindMessageByUserLoginSpecification(login), limit, offset)
                .stream()
                .map(message -> modelMapper.map(message, MessageDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<MessageDto> findMessagesByChatId(Long chatId) {
        return messageRepository.findBy(new FindMessageByChatIdSpecification(chatId))
                .stream()
                .map(message -> modelMapper.map(message, MessageDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<MessageDto> findMessagesByChatIdWithPagination(Long chatId, Integer limit, Integer offset) {
        if (limit < 0 || offset < 0) {
            throw new BadRequestException(MESSAGE_WRONG_PAGINATION_PARAMETERS);
        }
        return messageRepository.findByWithLimit(new FindMessageByChatIdSpecification(chatId), limit, offset)
                .stream()
                .map(message -> modelMapper.map(message, MessageDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public Long getMessagesCount() {
        return messageRepository.getFieldsCount();
    }

    private boolean isCurrentUserMessageAuthor(Message message, String userLogin){
        User user = message.getUser();
        String messageAuthor = user.getLogin();
        return messageAuthor.equals(userLogin);
    }

    private boolean isCurrentUserChatAdmin(Long id, String userLogin){
        boolean result = false;
        Optional<Membership> currentUserMembership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(userLogin, id));
        if(currentUserMembership.isPresent()){
            Membership membership = currentUserMembership.get();
            UserRole currentUserRole = membership.getUserRole();
            if(currentUserRole.equals(UserRole.ADMIN)){
                result = true;
            }
        }
        return result;
    }
}
