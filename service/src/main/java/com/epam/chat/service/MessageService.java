package com.epam.chat.service;

import com.epam.chat.model.dto.MessageCreateDto;
import com.epam.chat.model.dto.MessageUpdateDto;
import com.epam.chat.model.dto.response.MessageDto;

import java.util.List;

public interface MessageService {

    MessageDto findById(Long id);

    List<MessageDto> findAll();

    MessageDto save(MessageCreateDto message, String userLogin);

    MessageDto update(Long id, MessageUpdateDto message, String userLogin);

    void deleteById(Long id, String userLogin);

    List<MessageDto> findMessagesByUserLogin(String login);

    List<MessageDto> findMessagesByUserLoginWithPagination(String login, Integer limit, Integer offset);

    List<MessageDto> findMessagesByChatId(Long chatId);

    List<MessageDto> findMessagesByChatIdWithPagination(Long chatId, Integer limit, Integer offset);

    Long getMessagesCount();

}
