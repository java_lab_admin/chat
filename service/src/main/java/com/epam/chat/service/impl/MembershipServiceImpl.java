package com.epam.chat.service.impl;

import com.epam.chat.model.dto.ChatDtoEmbedded;
import com.epam.chat.model.dto.MembershipCreateDto;
import com.epam.chat.model.dto.UserLoginDtoEmbedded;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.model.entity.Chat;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.User;
import com.epam.chat.model.enums.ChatType;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.impl.chat.FindChatByIdSpecification;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByChatIdSpecification;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByUserLoginAndChatId;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByUserLoginSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByChatIdAndUserLoginSpecification;
import com.epam.chat.repository.specification.impl.user.FindUserByLoginSpecification;
import com.epam.chat.service.MembershipService;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.persistence.EntityTransaction;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class MembershipServiceImpl implements MembershipService {
    private static final String MEMBERSHIP_NOT_FOUND = "Membership is not found";
    private static final String USER_NOT_FOUND = "User login not found : ";
    private static final String CHAT_NOT_FOUND = "Chat id not found : ";
    private static final String ALREADY_REGISTERED = "Already registered in chat";
    private static final String USER_CANT_ADD_ANYONE = "User can't add anyone to group chat";
    private static final String USER_CANT_ADD_ADMIN = "User can't add Admin to chat";
    private static final String CANT_REGISTER_IN_GROUP_CHAT = "Can't register in group chat";
    private static final String CANT_REGISTER_AS_ADMIN = "Can't register as Admin";

    private ModelMapper modelMapper = new ModelMapper();

    private Repository<Membership> membershipRepository;
    private Repository<Chat> chatRepository;
    private Repository<User> userRepository;
    @Inject Repository<Message> messageRepository;

    @Inject
    public MembershipServiceImpl(Repository<Membership> membershipRepository, Repository<Chat> chatRepository,
                                 Repository<User> userRepository) {
        this.membershipRepository = membershipRepository;
        this.chatRepository = chatRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<MembershipDto> findByChatId(Long chatId) {
        List<Membership> memberships = membershipRepository.findBy(new FindMembershipByChatIdSpecification(chatId));
        return memberships
                .stream()
                .map(membership -> modelMapper.map(membership, MembershipDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<MembershipDto> findByUserLogin(String login) {
        List<Membership> memberships = membershipRepository.findBy(new FindMembershipByUserLoginSpecification(login));
        return memberships
                .stream()
                .map(membership -> modelMapper.map(membership, MembershipDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public MembershipDto findByChatIdAndUserLogin(Long chatId, String login) {
        Membership membership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(login, chatId))
                .orElseThrow(() -> new NotFoundException(MEMBERSHIP_NOT_FOUND));

        return modelMapper.map(membership, MembershipDto.class);
    }

    @Override
    public MembershipDto save(MembershipCreateDto membershipDto, String currentUserLogin) {
        UserLoginDtoEmbedded userloginDtoEmbedded = membershipDto.getUser();
        userRepository.findSingleBy(new FindUserByLoginSpecification(userloginDtoEmbedded.getLogin()))
                .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND + userloginDtoEmbedded.getLogin()));
        ChatDtoEmbedded chatDtoEmbedded = membershipDto.getChat();
        Long chatId = chatDtoEmbedded.getId();
        Chat chat = chatRepository.findSingleBy(new FindChatByIdSpecification(chatId))
                .orElseThrow(() -> new NotFoundException(CHAT_NOT_FOUND + chatDtoEmbedded.getId()));

        if (isUserChatMember(chatId, userloginDtoEmbedded.getLogin())) {
            throw new ForbiddenException(ALREADY_REGISTERED);
        }

        if (isUserChatMember(chatId, currentUserLogin)) {
            return addUserToChat(membershipDto, chat, currentUserLogin);
        }

        return registerToChat(membershipDto, chat);
    }

    @Override
    public void removeByChatIdAndUserLogin(Long chatId, String login) {
        Membership currentMembership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(login, chatId))
                .orElseThrow(() -> new NotFoundException(MEMBERSHIP_NOT_FOUND));
        List<Message> messages = messageRepository.findBy(new FindMessageByChatIdAndUserLoginSpecification(login, chatId));
        List<Membership> memberships = membershipRepository.findBy(new FindMembershipByChatIdSpecification(chatId));
        memberships.remove(currentMembership);
        List<Membership> admins = memberships.stream().filter(membership -> UserRole.ADMIN.equals(membership.getUserRole())).collect(Collectors.toList());
        membershipRepository.transactionManage(EntityTransaction::begin);
        if (memberships.size() > 0) {
            if (admins.isEmpty()) {
                Random random = new Random();
                Membership membership = memberships.get(random.nextInt(memberships.size() - 1));
                membership.setUserRole(UserRole.ADMIN);
                membershipRepository.save(membership);
            }
        } else {
            chatRepository.removeById(chatId);
        }
        messages.forEach(message -> messageRepository.remove(message));
        membershipRepository.remove(currentMembership);
        membershipRepository.transactionManage(EntityTransaction::commit);
    }

    @Override
    public boolean isUserChatMember(Long chatId, String login) {
        return membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(login, chatId)).isPresent();
    }

    private MembershipDto addUserToChat(MembershipCreateDto membershipCreateDto, Chat chat, String currentUserLogin) {
        Long chatId = chat.getId();
        ChatType chatType = chat.getType();
        Optional<Membership> optionalMembership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(currentUserLogin, chatId));
        Membership membership = optionalMembership.get();
        UserRole currentUserRole = membership.getUserRole();
        if (currentUserRole.equals(UserRole.USER)) {
            if (chatType.equals(ChatType.GROUP)) {
                throw new ForbiddenException(USER_CANT_ADD_ANYONE);
            }
            if (membershipCreateDto.getUserRole().equals(UserRole.ADMIN.name())) {
                throw new ForbiddenException(USER_CANT_ADD_ADMIN);
            }
        }
        membershipRepository.transactionManage(EntityTransaction::begin);
        Membership createdMembership = membershipRepository.save(modelMapper.map(membershipCreateDto, Membership.class));
        membershipRepository.transactionManage(EntityTransaction::commit);
        return modelMapper.map(createdMembership, MembershipDto.class);
    }

    private MembershipDto registerToChat(MembershipCreateDto membershipCreateDto, Chat chat) {
        if (ChatType.GROUP.equals(chat.getType())) {
            throw new ForbiddenException(CANT_REGISTER_IN_GROUP_CHAT);
        }
        if (UserRole.ADMIN.equals(UserRole.valueOf(membershipCreateDto.getUserRole()))) {
            throw new ForbiddenException(CANT_REGISTER_AS_ADMIN);
        }
        membershipRepository.transactionManage(EntityTransaction::begin);
        Membership createdMembership = membershipRepository.save(modelMapper.map(membershipCreateDto, Membership.class));
        membershipRepository.transactionManage(EntityTransaction::commit);
        return modelMapper.map(createdMembership, MembershipDto.class);
    }


}
