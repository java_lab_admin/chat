package com.epam.chat.service.impl;

import com.epam.chat.model.dto.UserDto;
import com.epam.chat.model.dto.UserLoginDtoEmbedded;
import com.epam.chat.model.dto.UserPasswordDtoEmbedded;
import com.epam.chat.model.entity.Chat;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.User;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByChatIdAndUserRoleSpecification;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByChatIdSpecification;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByUserLoginSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByChatIdSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByUserLoginSpecification;
import com.epam.chat.repository.specification.impl.user.FindUserByLoginSpecification;
import com.epam.chat.service.UserService;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.persistence.EntityTransaction;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class UserServiceImpl implements UserService {

    private static final String USER_NOT_FOUND = "User is not found";
    private static final String USER_ALREADY_EXISTS = "User with given login already exists : ";

    private ModelMapper modelMapper = new ModelMapper();

    private Repository<User> userRepository;
    private Repository<Membership> membershipRepository;
    private Repository<Message> messageRepository;
    private Repository<Chat> chatRepository;

    @Inject
    public UserServiceImpl(Repository<User> userRepository, Repository<Membership> membershipRepository,
                           Repository<Message> messageRepository, Repository<Chat> chatRepository) {
        this.userRepository = userRepository;
        this.membershipRepository = membershipRepository;
        this.messageRepository = messageRepository;
        this.chatRepository = chatRepository;
    }

    @Override
    public UserLoginDtoEmbedded findByLogin(String login) {
        User user = userRepository.findSingleBy(new FindUserByLoginSpecification(login))
                .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND));
        return modelMapper.map(user, UserLoginDtoEmbedded.class);
    }

    @Override
    public UserLoginDtoEmbedded save(UserDto userDto) {
        userRepository.findSingleBy(new FindUserByLoginSpecification(userDto.getLogin()))
                .ifPresent(x -> {
                    throw new BadRequestException(USER_ALREADY_EXISTS + x.getLogin());
                });

        userRepository.transactionManage(EntityTransaction::begin);
        User user = userRepository.save(modelMapper.map(userDto, User.class));
        userRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(user, UserLoginDtoEmbedded.class);
    }

    @Override
    public UserLoginDtoEmbedded updatePassword(UserPasswordDtoEmbedded userDto, String login) {
        User user = userRepository.findSingleBy(new FindUserByLoginSpecification(login))
                .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND));
        user.setPassword(userDto.getPassword());

        userRepository.transactionManage(EntityTransaction::begin);
        User updatedUser = userRepository.save(user);
        userRepository.transactionManage(EntityTransaction::commit);
        return modelMapper.map(updatedUser, UserLoginDtoEmbedded.class);
    }

    @Override
    public void delete(String login) {
        List<Membership> memberships = membershipRepository.findBy(new FindMembershipByUserLoginSpecification(login));
        List<Message> messages = messageRepository.findBy(new FindMessageByUserLoginSpecification(login));
        User user = userRepository.findSingleBy(new FindUserByLoginSpecification(login))
                .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND));
        List<Chat> onlyAdminChats = findChatsWhereOnlyAdmin(memberships);
        List<Membership> adminMemberships = setNewRandomAdmins(memberships);

        userRepository.transactionManage(EntityTransaction::begin);
        removeChatsRelatedInfo(onlyAdminChats);
        chatRepository.remove(onlyAdminChats);
        membershipRepository.save(adminMemberships);
        membershipRepository.remove(memberships);
        messageRepository.remove(messages);
        userRepository.remove(user);
        userRepository.transactionManage(EntityTransaction::commit);
    }

    private List<Membership> setNewRandomAdmins(List<Membership> memberships) {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        List<Membership> newAdmins = new ArrayList<>();
        for (Membership membership : memberships) {
            if (!membership.getUserRole().equals(UserRole.ADMIN)) {
                continue;
            }
            Chat chat = membership.getChat();
            List<Membership> membershipList = membershipRepository.findBy(new FindMembershipByChatIdAndUserRoleSpecification(chat.getId(), UserRole.USER));
            if (!membershipList.isEmpty()) {
                int memberIndex = random.nextInt(0, membershipList.size());
                Membership newAdmin = membershipList.get(memberIndex);
                newAdmin.setUserRole(UserRole.ADMIN);
                newAdmins.add(newAdmin);
            }
        }
        return newAdmins;
    }

    private List<Chat> findChatsWhereOnlyAdmin(List<Membership> memberships) {
        List<Chat> chats = new ArrayList<>();
        for (Membership membership : memberships) {
            Chat chat = membership.getChat();
            List<Membership> membershipList = membershipRepository.findBy(new FindMembershipByChatIdSpecification(chat.getId()));
            if (membershipList.size() == 1) {
                chats.add(chat);
            }
        }
        return chats;
    }

    private void removeChatsRelatedInfo(List<Chat> chats) {
        for (Chat chat : chats) {
            Long chatId = chat.getId();
            List<Membership> memberships = membershipRepository.findBy(new FindMembershipByChatIdSpecification(chatId));
            List<Message> messages = messageRepository.findBy(new FindMessageByChatIdSpecification(chatId));

            messageRepository.remove(messages);
            membershipRepository.remove(memberships);
        }
    }
}
