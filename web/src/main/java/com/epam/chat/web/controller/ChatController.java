package com.epam.chat.web.controller;

import com.epam.chat.model.dto.*;
import com.epam.chat.model.dto.response.ChatDto;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.service.ChatService;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.MessageService;
import com.epam.chat.web.security.filter.binding.Secured;
import io.swagger.annotations.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;

@Path("/chats")
@Api(value = "Chat", authorizations = {
        @Authorization(value = "basicAuth")
})
@Secured
public class ChatController {

    private static final String FORBIDDEN_ERROR_MESSAGE = "Only admin can perform such actions";
    private static final String CHAT_CREATED = "/chats/%d";

    private ChatService chatService;
    private MembershipService membershipService;
    private MessageService messageService;

    @Inject
    public ChatController(ChatService chatService, MembershipService membershipService, MessageService messageService){
        this.chatService = chatService;
        this.membershipService = membershipService;
        this.messageService = messageService;
    }

    @ApiOperation(value = "Find chat by id", response = ChatDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found chat by id"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist")
    })
    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findChatById(@ApiParam(value = "Id to find chat", required = true) @PathParam("id") long id) {
        ChatDto chat = chatService.findById(id);
        return Response.ok(chat).build();
    }

    @ApiOperation(value = "Find chat messages by chat id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found chat messages"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist"),
            @ApiResponse(code = 403, message = "Forbidden. Only chat members can see chat messages")

    })
    @GET
    @Path("/{id}/messages")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findChatMessages(@PathParam("id") Long id,
                                     @Context SecurityContext securityContext,
                                     @QueryParam("limit") Integer limit,
                                     @QueryParam("offset") @DefaultValue("0") Integer offset){
        Principal userPrincipal = securityContext.getUserPrincipal();
        String currentUser = userPrincipal.getName();
        if (!membershipService.isUserChatMember(id, currentUser)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }

        if (limit == null) {
            limit = messageService.getMessagesCount().intValue();
        }

        List<MessageDto> chatMessages = messageService.findMessagesByChatIdWithPagination(id, limit, offset);
        return Response.ok(chatMessages).build();

    }

    @ApiOperation(value = "Find all chats", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found all chats")
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findAllChats() {
        List<ChatDto> allChats = chatService.findAll();
        return Response.ok(allChats).build();
    }

    @ApiOperation(value = "Add new chat", response = ChatDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully added chat"),
            @ApiResponse(code = 400, message = "Invalid chat fields")
    })
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addChat(@ApiParam(value = "Chat object to save in database", required = true) @Valid ChatCreateOrUpdateDto chat,
                            @Context SecurityContext securityContext) throws URISyntaxException {
        Principal principal = securityContext.getUserPrincipal();
        ChatDto createdChat = chatService.save(chat, principal.getName());
        String path = String.format(CHAT_CREATED, createdChat.getId());
        URI uri = new URI(path);
        return Response.created(uri).entity(createdChat).build();
    }

    @ApiOperation("Delete chat")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully deleted chat"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist"),
            @ApiResponse(code = 403, message = "Forbidden. Only chat members can delete chats")
    })
    @DELETE
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteChat(@ApiParam(value = "Id to delete chat", required = true) @PathParam("id") long id,
                               @Context SecurityContext securityContext) {
        Principal userPrincipal = securityContext.getUserPrincipal();
        if(!isUserChatAdmin(id, userPrincipal)){
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        chatService.removeById(id);
        return Response.noContent().build();
    }

    @ApiOperation(value = "Update chat", response = ChatDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated chat"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist"),
            @ApiResponse(code = 400, message = "Invalid chat fields"),
            @ApiResponse(code = 403, message = "Forbidden. Only admin can update chats")
    })
    @PUT
    @Path("/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateChat(@ApiParam(value = "Id to update chat", required = true) @PathParam("id") long id,
                               @ApiParam(value = "Chat object to update in database", required = true) @Valid ChatCreateOrUpdateDto chat,
                               @Context SecurityContext securityContext) {
        Principal userPrincipal = securityContext.getUserPrincipal();
        if(!isUserChatAdmin(id, userPrincipal)){
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        ChatDto updatedChat = chatService.update(id, chat);
        return Response.ok(updatedChat).build();
    }

    private boolean isUserChatAdmin(long id, Principal userPrincipal) {
        String currentUser = userPrincipal.getName();
        MembershipDto membership = membershipService.findByChatIdAndUserLogin(id, currentUser);
        UserRole currentUserRole = membership.getUserRole();
        return currentUserRole.equals(UserRole.ADMIN);
    }
}
