package com.epam.chat.web.controller;

import com.epam.chat.model.dto.ChatDtoEmbedded;
import com.epam.chat.model.dto.MembershipCreateDto;
import com.epam.chat.model.dto.MembershipDtoEmbedded;
import com.epam.chat.model.dto.UserLoginDtoEmbedded;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.service.MembershipService;
import com.epam.chat.web.security.filter.binding.Secured;
import io.swagger.annotations.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;

@Api(value = "Chat", authorizations = {
        @Authorization(value = "basicAuth")
})
@Path("/chats")
@Secured
public class MembershipController {

    private static final String FORBIDDEN_ERROR_MESSAGE = "Access denied";

    private MembershipService membershipService;

    @Inject
    public MembershipController(MembershipService membershipService) {
        this.membershipService = membershipService;
    }

    @ApiOperation(value = "Find chat memberships", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found chat memberships"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist"),
            @ApiResponse(code = 403, message = "Forbidden. Only chat members can see chat memberships")

    })
    @GET
    @Path("/{id}/memberships")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findChatMemberships(@PathParam("id") Long id, @Context SecurityContext securityContext){
        Principal userPrincipal = securityContext.getUserPrincipal();
        String currentUser = userPrincipal.getName();
        if (!membershipService.isUserChatMember(id, currentUser)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        List<MembershipDto> chatMemberships = membershipService.findByChatId(id);
        return Response.ok(chatMemberships).build();
    }

    @POST
    @ApiOperation(value = "Add new membership to chat", response = MembershipDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully added membership"),
            @ApiResponse(code = 400, message = "Invalid chat fields"),
            @ApiResponse(code = 403, message = "Forbidden add membership"),
            @ApiResponse(code = 404, message = "User not found"),
            @ApiResponse(code = 404, message = "Chat not found")
    })
    @Path("/{chatId}/memberships")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createMembership(@PathParam(value = "chatId") Long chatId,
                                     @ApiParam(value = "Membership object to save in database", required = true) @Valid MembershipDtoEmbedded membershipDtoEmbedded,
                                     @Context SecurityContext securityContext) throws URISyntaxException {
        Principal principal = securityContext.getUserPrincipal();
        String currentUserLogin = principal.getName();
        UserLoginDtoEmbedded userLoginDtoEmbedded = membershipDtoEmbedded.getUser();

        MembershipCreateDto membershipCreateDto = new MembershipCreateDto();
        membershipCreateDto.setUser(userLoginDtoEmbedded);
        ChatDtoEmbedded chatDtoEmbedded = new ChatDtoEmbedded();
        chatDtoEmbedded.setId(chatId);
        membershipCreateDto.setChat(chatDtoEmbedded);
        membershipCreateDto.setUserRole(membershipDtoEmbedded.getUserRole());


        MembershipDto membershipDto = membershipService.save(membershipCreateDto, currentUserLogin);
        return Response.status(201).entity(membershipDto).build();
    }

    @ApiOperation("Delete membership")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully deleted membership"),
            @ApiResponse(code = 404, message = "Membership with given chat id and username doesn't exist")
    })
    @DELETE
    @Path("/{chatId}/users/{userToDelete}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteMembership(@PathParam("chatId") long id,
                                     @PathParam("userToDelete") String userToDelete,
                                     @Context SecurityContext securityContext) {
        Principal userPrincipal = securityContext.getUserPrincipal();
        String currentUser = userPrincipal.getName();
        if (!membershipService.isUserChatMember(id, currentUser) && !membershipService.isUserChatMember(id, userToDelete)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        MembershipDto membershipDto = membershipService.findByChatIdAndUserLogin(id, currentUser);
        if (membershipDto.getUserRole().equals(UserRole.USER)) {
            if (!userToDelete.equals(currentUser)) {
                throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
            }
        }
        membershipService.removeByChatIdAndUserLogin(id, userToDelete);
        return Response.noContent().build();
    }
}
