package com.epam.chat.web.controller;

import com.epam.chat.model.dto.MessageCreateDto;
import com.epam.chat.model.dto.MessageUpdateDto;
import com.epam.chat.model.dto.response.ChatDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.MessageService;
import com.epam.chat.web.security.filter.binding.Secured;
import io.swagger.annotations.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;

@Path("/messages")
@Api(value = "Chat", authorizations = {
        @Authorization(value = "basicAuth")
})
@Secured
public class MessageController {

    private static final String FORBIDDEN_ERROR = "Request forbidden";
    private static final String MESSAGE_CREATED = "/messages/%d";

    private MessageService messageService;
    private MembershipService membershipService;

    @Inject
    public MessageController(MessageService messageService, MembershipService membershipService) {
        this.messageService = messageService;
        this.membershipService = membershipService;
    }

    @ApiOperation(value = "Get message by id", response = MessageDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found message"),
            @ApiResponse(code = 404, message = "Message with given id not found"),
            @ApiResponse(code = 403, message = "Only chat member can see this message")
    })
    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findMessageById(@ApiParam(value = "Message id to get object", required = true) @PathParam("id") long id, @Context SecurityContext securityContext) {
        MessageDto message = messageService.findById(id);
        if (!isThereUserAccess(message, securityContext.getUserPrincipal())) {
            throw new ForbiddenException(FORBIDDEN_ERROR);
        }
        return Response.ok(message).build();
    }

    @ApiOperation(value = "Get all user's messages", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found all user's messages"),
            @ApiResponse(code = 403, message = "User is not member of chat")
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findMessagesByCurrentUser(@Context SecurityContext securityContext,
                                              @QueryParam("limit") Integer limit,
                                              @QueryParam("offset") @DefaultValue("0") Integer offset) {
        if (limit == null) {
            limit = messageService.getMessagesCount().intValue();
        }

        Principal principal = securityContext.getUserPrincipal();
        String login = principal.getName();
        List<MessageDto> messages = messageService.findMessagesByUserLoginWithPagination(login, limit, offset);
        return Response.ok(messages).build();
    }

    @ApiOperation(value = "Add new message", response = MessageDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully added message"),
            @ApiResponse(code = 400, message = "Invalid message fields values"),
            @ApiResponse(code = 403, message = "Not current logged in user in request body"),
            @ApiResponse(code = 403, message = "User is not member of chat")
    })
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addMessage(@ApiParam(value = "Message object to save object in database") @Valid MessageCreateDto message,
                               @Context SecurityContext securityContext) throws URISyntaxException {
        Principal userPrincipal = securityContext.getUserPrincipal();
        MessageDto createdMessage = messageService.save(message, userPrincipal.getName());
        String path = String.format(MESSAGE_CREATED, createdMessage.getId());
        URI uri = new URI(path);
        return Response.created(uri).entity(createdMessage).build();
    }

    @ApiOperation("Delete message")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully deleted message"),
            @ApiResponse(code = 404, message = "Message with given id doesn't exist")
    })
    @DELETE
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteMessage(@ApiParam(value = "Message id to delete object", required = true) @PathParam("id") long id,
                                  @Context SecurityContext securityContext) {
        Principal userPrincipal = securityContext.getUserPrincipal();
        messageService.deleteById(id, userPrincipal.getName());
        return Response.noContent().build();
    }

    @ApiOperation(value = "Update message", response = MessageDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated message"),
            @ApiResponse(code = 404, message = "Message with given id is not found"),
            @ApiResponse(code = 400, message = "Invalid message fields values")
    })
    @PUT
    @Path("/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateMessage(@PathParam("id") long id, @Valid MessageUpdateDto message, @Context SecurityContext securityContext) {
        Principal principal = securityContext.getUserPrincipal();
        MessageDto updatedMessage = messageService.update(id, message, principal.getName());
        return Response.ok(updatedMessage).build();
    }

    private boolean isThereUserAccess(MessageDto messageDto, Principal principal) {
        ChatDto chatDto = messageDto.getChat();
        long chatId = chatDto.getId();
        String currentUserLogin = principal.getName();
        return membershipService.isUserChatMember(chatId, currentUserLogin);
    }
}
