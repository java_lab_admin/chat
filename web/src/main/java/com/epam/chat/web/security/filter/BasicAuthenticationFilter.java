package com.epam.chat.web.security.filter;

import com.epam.chat.model.entity.User;
import com.epam.chat.service.AuthService;
import com.epam.chat.web.security.context.UserSecurityContext;
import com.epam.chat.web.security.filter.binding.Secured;
import org.glassfish.jersey.internal.util.Base64;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Optional;
import java.util.StringTokenizer;

@Secured
@Provider
public class BasicAuthenticationFilter implements ContainerRequestFilter {


    private static final String BASIC_AUTH_REGEX = "^(B|b)asic ";
    private static final String NOT_HAVE_ACCESS_MESSAGE = "You cannot access this resource";

    @Inject
    private AuthService authService;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        String authHeader = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if(authHeader == null || authHeader.isEmpty()) {
            throw new NotAuthorizedException(NOT_HAVE_ACCESS_MESSAGE);
        }
        final String encodedUserPassword = authHeader.replaceFirst(BASIC_AUTH_REGEX, "");
        String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));
        final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
        final String login = tokenizer.nextToken();
        final String password = tokenizer.nextToken();
        Optional<User> userInDB = authService.findByLoginAndPassword(login, password);
        if (!userInDB.isPresent()) {
            throw new NotAuthorizedException(NOT_HAVE_ACCESS_MESSAGE);
        }
        String scheme = containerRequestContext.getUriInfo().getRequestUri().getScheme();
        containerRequestContext.setSecurityContext(new UserSecurityContext(userInDB.get(), scheme));
    }
}
