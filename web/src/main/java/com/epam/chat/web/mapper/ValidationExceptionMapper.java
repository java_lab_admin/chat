package com.epam.chat.web.mapper;

import com.epam.chat.model.dto.response.ErrorDto;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(prepareMessage(exception))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    private List<ErrorDto> prepareMessage(ConstraintViolationException exception) {
        List<ErrorDto> errorDtos = new ArrayList<>();
        for (ConstraintViolation<?> cv : exception.getConstraintViolations()) {
            errorDtos.add(new ErrorDto(Response.Status.BAD_REQUEST.getStatusCode(), cv.getMessage(), cv.getMessage()));
        }
        return errorDtos;
    }
}
