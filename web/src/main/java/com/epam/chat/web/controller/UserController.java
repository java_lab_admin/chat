package com.epam.chat.web.controller;

import com.epam.chat.model.dto.UserDto;
import com.epam.chat.model.dto.UserLoginDtoEmbedded;
import com.epam.chat.model.dto.UserPasswordDtoEmbedded;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.UserService;
import com.epam.chat.web.security.filter.binding.Secured;
import io.swagger.annotations.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;

@Path("/users")
@Api("Chat")
public class UserController  {

    private static final String FORBIDDEN_ERROR_MESSAGE = "Request forbidden";
    private static final String USER_CREATED = "/users/%s";

    private UserService userService;
    private MembershipService membershipService;

    @Inject
    public UserController(UserService userService, MembershipService membershipService) {
        this.userService = userService;
        this.membershipService = membershipService;
    }

    @ApiOperation(value = "Add new user", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully added user"),
            @ApiResponse(code = 400, message = "Invalid user fields"),
            @ApiResponse(code = 400, message = "User with given login already exists")
    })
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addUser(@Valid UserDto userDto) throws URISyntaxException {
        UserLoginDtoEmbedded createdUser = userService.save(userDto);
        String path = String.format(USER_CREATED, createdUser.getLogin());
        URI uri = new URI(path);
        return Response.created(uri).entity(createdUser).build();
    }

    @ApiOperation(value = "Find user's memberships", response = List.class, authorizations =  {
            @Authorization(value = "basicAuth")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found user's memberships")
    })
    @GET
    @Secured
    @Path("/memberships")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUsersMemberships(@Context SecurityContext securityContext){
        Principal userPrincipal = securityContext.getUserPrincipal();
        String userLogin = userPrincipal.getName();
        List<MembershipDto> usersMemberships = membershipService.findByUserLogin(userLogin);
        return Response.ok(usersMemberships).build();
    }

    @ApiOperation(value = "Edit user's password", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Password is changed successfully"),
            @ApiResponse(code = 400, message = "Invalid password field value"),
            @ApiResponse(code = 404, message = "User hasn't been found")
    })
    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUserPassword(@Valid UserPasswordDtoEmbedded userPasswordDtoEmbedded, @Context SecurityContext securityContext) {
        Principal principal = securityContext.getUserPrincipal();
        UserLoginDtoEmbedded updatedUserDto = userService.updatePassword(userPasswordDtoEmbedded, principal.getName());
        return Response.ok(updatedUserDto).build();
    }

    @ApiOperation("Delete user")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "User has been deleted successfully"),
            @ApiResponse(code = 403, message = "User can't delete other users"),
            @ApiResponse(code = 404, message = "User hasn't been found")
    })
    @DELETE
    @Path("/{login}")
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAccount(@PathParam("login") String login, @Context SecurityContext securityContext) {
        Principal principal = securityContext.getUserPrincipal();
        String currentUserLogin = principal.getName();
        if (!currentUserLogin.equals(login)){
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        userService.delete(login);
        return Response.noContent().build();
    }

}
