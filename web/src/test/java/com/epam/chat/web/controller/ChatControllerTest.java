package com.epam.chat.web.controller;

import com.epam.chat.model.dto.ChatCreateOrUpdateDto;
import com.epam.chat.model.dto.response.ChatDto;
import com.epam.chat.model.dto.response.ErrorDto;
import com.epam.chat.model.entity.User;
import com.epam.chat.service.AuthService;
import com.epam.chat.service.ChatService;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.impl.AuthServiceImpl;
import com.epam.chat.service.impl.ChatServiceImpl;
import com.epam.chat.service.impl.MembershipServiceImpl;
import com.epam.chat.service.impl.MessageServiceImpl;
import com.epam.chat.web.mapper.GenericExceptionMapper;
import com.epam.chat.web.security.filter.BasicAuthenticationFilter;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class ChatControllerTest extends JerseyTest {

    private static final String USERNAME = "username";
    private static final String PASSWORD = "secret";

    @Mock
    private ChatServiceImpl chatServiceMock;

    @Mock
    private MembershipServiceImpl membershipServiceMock;

    @Mock
    private AuthServiceImpl authServiceMock;

    @Mock
    private MessageServiceImpl messageServiceMock;

    private HttpAuthenticationFeature authenticationFeature;

    private User currentUser;

    @Override
    protected Application configure() {
        MockitoAnnotations.initMocks(this);
        authenticationFeature = HttpAuthenticationFeature.basic( USERNAME, PASSWORD);
        currentUser = new User();
        currentUser.setLogin(USERNAME);
        currentUser.setPassword(PASSWORD);

        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);

        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.register(new ChatController(chatServiceMock, membershipServiceMock, messageServiceMock));
        resourceConfig.register(JacksonFeature.class);
        resourceConfig.register(GenericExceptionMapper.class);
        resourceConfig.register(BasicAuthenticationFilter.class);
        resourceConfig.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(chatServiceMock).to(ChatService.class).in(RequestScoped.class);
                bind(membershipServiceMock).to(MembershipService.class).in(RequestScoped.class);
                bind(authServiceMock).proxy(true).proxyForSameScope(false).to(AuthService.class).in(RequestScoped.class);
            }
        });
        return resourceConfig;
    }

    @Test
    public void testFindingChatByIdAuthorized(){
        ChatDto chatDto = new ChatDto();
        chatDto.setName("Chat name");

        when(chatServiceMock.findById(1L)).thenReturn(chatDto);
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));

        Response response = target("chats/1").register(authenticationFeature).request().get();

        assertEquals("Http Response should be 200: ", Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));

        ChatDto responseMessage = response.readEntity(ChatDto.class);
        assertNotNull(responseMessage);
        assertEquals("Chat name:", "Chat name", responseMessage.getName());
    }

    @Test
    public void testFindingChatByIdNotAuthorized(){
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.empty());

        Response response = target("chats/1").register(authenticationFeature).request().get();
        ErrorDto errorResponse = response.readEntity(ErrorDto.class);
        assertNotNull("ErrorDto can't be null: ", errorResponse);
        assertEquals("Http Response should be 401: ", Response.Status.UNAUTHORIZED.getStatusCode(), errorResponse.getCode());
        assertEquals("Error message: ", "Unauthorized", errorResponse.getMessage());
        assertEquals("Error description: ", "HTTP 401 Unauthorized",  errorResponse.getDescription());
    }

    @Test
    public void testAddingChatAuthorized(){
        ChatCreateOrUpdateDto chatDto = new ChatCreateOrUpdateDto();
        chatDto.setName("Chat name");
        ChatDto savedChat = new ChatDto();
        savedChat.setName("Saved chat");
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));

        Response response = target("chats").register(authenticationFeature).request().post(Entity.entity(chatDto, MediaType.APPLICATION_JSON));

        assertEquals("Http Response should be 201: ", Response.Status.CREATED.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));

        ChatDto responseMessage = response.readEntity(ChatDto.class);
        assertNotNull(responseMessage);
        assertEquals("Chat name:", "Saved chat", responseMessage.getName());
    }

    @Test
    public void testAddingChatNotAuthorized(){
        ChatCreateOrUpdateDto chatDto = new ChatCreateOrUpdateDto();
        chatDto.setName("Chat name");
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.empty());

        Response response = target("chats").register(authenticationFeature).request().post(Entity.entity(chatDto, MediaType.APPLICATION_JSON));
        ErrorDto errorResponse = response.readEntity(ErrorDto.class);
        assertNotNull("ErrorDto can't be null: ", errorResponse);
        assertEquals("Http Response should be 401: ", Response.Status.UNAUTHORIZED.getStatusCode(), errorResponse.getCode());
        assertEquals("Error message: ", "Unauthorized", errorResponse.getMessage());
        assertEquals("Error description: ", "HTTP 401 Unauthorized", errorResponse.getDescription());
    }

    @Test
    public void testDeletingChatWithNoRights(){
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));
        when(membershipServiceMock.isUserChatMember(1L, USERNAME)).thenReturn(false);

        Response response = target("chats/1").register(authenticationFeature).request().delete();
        ErrorDto error = response.readEntity(ErrorDto.class);
        assertNotNull("ErrorDto can't be null: ", error);
        assertEquals("Http Response should be 403: ", Response.Status.FORBIDDEN.getStatusCode(), error.getCode());
        assertEquals("Error message: ", "Forbidden", error.getMessage());
        assertEquals("Error description: ", "Only chat members can perform such actions", error.getDescription());
    }

    @Test
    public void testDeletingChatHavingRights(){
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));
        when(membershipServiceMock.isUserChatMember(1L, USERNAME)).thenReturn(true);
        doNothing().when(chatServiceMock).removeById(1L);

        Response response = target("chats/1").register(authenticationFeature).request().delete();
        assertEquals("Http Response should be 204: ", Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testDeletingNotExistingChatHavingRights(){
        String exceptionMessage = "Chat with such id doesn't exist";
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));
        when(membershipServiceMock.isUserChatMember(1L, USERNAME)).thenReturn(true);
        doThrow(new NotFoundException(exceptionMessage)).when(chatServiceMock).removeById(1L);

        Response response = target("chats/1").register(authenticationFeature).request().delete();
        assertEquals("Http Response should be 404", Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));

        ErrorDto errorResponse = response.readEntity(ErrorDto.class);
        assertEquals("Error message: ", exceptionMessage, errorResponse.getDescription());
    }

    @Test
    public void testUpdatingChatNotAuthorized(){
        ChatCreateOrUpdateDto chatDto = new ChatCreateOrUpdateDto();
        chatDto.setName("Chat name");
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.empty());

        Response response = target("chats/33").register(authenticationFeature).request().put(Entity.entity(chatDto, MediaType.APPLICATION_JSON));
        ErrorDto errorResponse = response.readEntity(ErrorDto.class);
        assertNotNull("ErrorDto can't be null: ", errorResponse);
        assertEquals("Http Response should be 401: ", Response.Status.UNAUTHORIZED.getStatusCode(), errorResponse.getCode());
        assertEquals("Error message: ", "Unauthorized", errorResponse.getMessage());
        assertEquals("Error description: ", "HTTP 401 Unauthorized",  errorResponse.getDescription());
    }

    @Test
    public void testUpdatingChatWithNoRights(){
        ChatCreateOrUpdateDto chatDto = new ChatCreateOrUpdateDto();
        chatDto.setName("Chat name");
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));
        when(membershipServiceMock.isUserChatMember(1L, USERNAME)).thenReturn(false);

        Response response = target("chats/33").register(authenticationFeature).request().put(Entity.entity(chatDto, MediaType.APPLICATION_JSON));
        ErrorDto error = response.readEntity(ErrorDto.class);
        assertNotNull("ErrorDto can't be null: ", error);
        assertEquals("Http Response should be 403: ", Response.Status.FORBIDDEN.getStatusCode(), error.getCode());
        assertEquals("Error message: ", "Forbidden", error.getMessage());
        assertEquals("Error description: ", "Only chat members can perform such actions", error.getDescription());
    }

    @Test
    public void testUpdatingChatHavingRights(){
        ChatCreateOrUpdateDto chatDto = new ChatCreateOrUpdateDto();
        chatDto.setName("Chat name");
        ChatDto updatedChatDto = new ChatDto();
        updatedChatDto.setName("Updated hat name");
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));
        when(membershipServiceMock.isUserChatMember(33L, USERNAME)).thenReturn(true);
        when(chatServiceMock.update(33L, chatDto)).thenReturn(updatedChatDto);

        Response response = target("chats/33").register(authenticationFeature).request().put(Entity.entity(chatDto, MediaType.APPLICATION_JSON));

        assertEquals("Http Response should be 200: ", Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));

        ChatDto responseChat = response.readEntity(ChatDto.class);
        assertNotNull(responseChat);
        assertEquals("Message text:", updatedChatDto.getName(), responseChat.getName());
    }

    @Test
    public void testUpdatingNotExistingChatHavingRights(){
        String exceptionMessage = "Chat with such id doesn't exist";
        ChatCreateOrUpdateDto chatDto = new ChatCreateOrUpdateDto();
        chatDto.setName("Chat name");
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));
        when(membershipServiceMock.isUserChatMember(33L, USERNAME)).thenReturn(true);
        when(chatServiceMock.update(33L, chatDto)).thenThrow(new NotFoundException(exceptionMessage));

        Response response = target("chats/33").register(authenticationFeature).request().put(Entity.entity(chatDto, MediaType.APPLICATION_JSON));

        ErrorDto errorResponse = response.readEntity(ErrorDto.class);
        assertNotNull(errorResponse);
        assertEquals("Error message: ", exceptionMessage, errorResponse.getDescription());
    }
}
