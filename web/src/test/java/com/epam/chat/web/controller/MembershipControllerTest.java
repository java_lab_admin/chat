package com.epam.chat.web.controller;

import com.epam.chat.model.dto.ChatDtoEmbedded;
import com.epam.chat.model.dto.MembershipCreateDto;
import com.epam.chat.model.dto.UserLoginDtoEmbedded;
import com.epam.chat.model.dto.response.ChatDto;
import com.epam.chat.model.dto.response.ErrorDto;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.model.entity.User;
import com.epam.chat.service.AuthService;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.impl.AuthServiceImpl;
import com.epam.chat.service.impl.MembershipServiceImpl;
import com.epam.chat.web.mapper.GenericExceptionMapper;
import com.epam.chat.web.security.filter.BasicAuthenticationFilter;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class MembershipControllerTest extends JerseyTest {

    private static final String USERNAME = "username";
    private static final String PASSWORD = "secret";

    @Mock
    private MembershipServiceImpl membershipServiceMock;

    @Mock
    private AuthServiceImpl authServiceMock;

    private HttpAuthenticationFeature authenticationFeature;

    private User currentUser;

    @Override
    protected Application configure() {
        MockitoAnnotations.initMocks(this);
        authenticationFeature = HttpAuthenticationFeature.basic( USERNAME, PASSWORD);
        currentUser = new User();
        currentUser.setLogin(USERNAME);
        currentUser.setPassword(PASSWORD);

        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);

        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.register(new MembershipController(membershipServiceMock));
        resourceConfig.register(JacksonFeature.class);
        resourceConfig.register(GenericExceptionMapper.class);
        resourceConfig.register(BasicAuthenticationFilter.class);
        resourceConfig.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(membershipServiceMock).to(MembershipService.class).in(RequestScoped.class);
                bind(authServiceMock).proxy(true).proxyForSameScope(false).to(AuthService.class).in(RequestScoped.class);
            }
        });
        return resourceConfig;
    }

    @Test
    public void testNotAuthorizedAddingMembership(){
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.empty());

        Response response = target("memberships").register(authenticationFeature).request().post(Entity.json(new MembershipCreateDto()));
        ErrorDto errorResponse = response.readEntity(ErrorDto.class);
        assertNotNull("ErrorDto can't be null: ", errorResponse);
        assertEquals("Http Response should be 401: ", Response.Status.UNAUTHORIZED.getStatusCode(), errorResponse.getCode());
        assertEquals("Error message: ", "Unauthorized", errorResponse.getMessage());
        assertEquals("Error description: ", "HTTP 401 Unauthorized",  errorResponse.getDescription());
    }

    @Test
    public void testAuthorizedAddingMembership(){
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));
        MembershipCreateDto membership = new MembershipCreateDto();
        UserLoginDtoEmbedded userDto = new UserLoginDtoEmbedded();
        userDto.setLogin("username");
        membership.setUser(userDto);
        ChatDtoEmbedded chatDto = new ChatDtoEmbedded();
        chatDto.setId(1L);
        //membership.setChat(chatDto);

        MembershipDto membershipDtoToReturn = new MembershipDto();
        UserLoginDtoEmbedded user = new UserLoginDtoEmbedded();
        user.setLogin("login");
        membershipDtoToReturn.setUser(user);
        membershipDtoToReturn.setChat(new ChatDto());

        Response response = target("memberships").register(authenticationFeature).request().post(Entity.entity(membership, MediaType.APPLICATION_JSON));

        assertEquals("Http Response should be 201: ", Response.Status.CREATED.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));

        MembershipDto membershipResponse = response.readEntity(MembershipDto.class);
        assertEquals("Membership username: ", membershipDtoToReturn.getUser().getLogin(), membershipResponse.getUser().getLogin());
    }

    @Test
    public void testAuthorizedAddingMembershipWithInvalidUsername(){
        when(authServiceMock.findByLoginAndPassword(USERNAME, PASSWORD)).thenReturn(Optional.of(currentUser));
        MembershipCreateDto membership = new MembershipCreateDto();
        UserLoginDtoEmbedded userDto = new UserLoginDtoEmbedded();
        userDto.setLogin("incorrect");
        membership.setUser(userDto);
        ChatDtoEmbedded chatDto = new ChatDtoEmbedded();
        chatDto.setId(1L);
        //membership.setChat(chatDto);

        Response response = target("memberships").register(authenticationFeature).request().post(Entity.entity(membership, MediaType.APPLICATION_JSON));

        ErrorDto errorResponse = response.readEntity(ErrorDto.class);
        assertNotNull("ErrorDto can't be null: ", errorResponse);
        assertEquals("Http Response should be 403: ", Response.Status.FORBIDDEN.getStatusCode(), errorResponse.getCode());
        assertEquals("Error message: ", "Forbidden", errorResponse.getMessage());
        assertEquals("Error description: ", "Access denied", errorResponse.getDescription());
    }
}
