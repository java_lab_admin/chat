package com.epam.chat.web.controller;

import com.epam.chat.service.MembershipService;
import com.epam.chat.service.MessageService;
import com.epam.chat.service.impl.MembershipServiceImpl;
import com.epam.chat.service.impl.MessageServiceImpl;
import com.epam.chat.web.mapper.GenericExceptionMapper;
import com.epam.chat.web.mapper.ValidationExceptionMapper;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.glassfish.jersey.test.grizzly.GrizzlyTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Application;

public class MessageControllerTest extends JerseyTest {

    @Mock
    private MessageServiceImpl messageServiceMock;

    @Mock
    private MembershipServiceImpl membershipServiceMock;

    @Override
    protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
        return new GrizzlyTestContainerFactory();
    }

    @Override
    protected Application configure() {
        MockitoAnnotations.initMocks(this);
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);

        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.register(new MessageController(messageServiceMock, membershipServiceMock));
        resourceConfig.register(JacksonFeature.class);
        resourceConfig.register(GenericExceptionMapper.class);
        resourceConfig.register(ValidationExceptionMapper.class);
        resourceConfig.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(messageServiceMock).to(MessageService.class).in(RequestScoped.class);
                bind(membershipServiceMock).to(MembershipService.class).in(RequestScoped.class);
            }
        });
        return resourceConfig;
    }

}
