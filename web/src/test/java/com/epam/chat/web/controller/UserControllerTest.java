package com.epam.chat.web.controller;

import com.epam.chat.model.dto.UserDto;
import com.epam.chat.service.ChatService;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.UserService;
import com.epam.chat.web.mapper.GenericExceptionMapper;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.validation.internal.ValidationExceptionMapper;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.glassfish.jersey.test.grizzly.GrizzlyTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class UserControllerTest extends JerseyTest {

    @Mock
    private UserService userServiceMock;

    @Mock
    private MembershipService membershipServiceMock;

    @Override
    protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
        return new GrizzlyTestContainerFactory();
    }

    @Override
    protected Application configure() {
        MockitoAnnotations.initMocks(this);
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);

        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.register(new UserController(userServiceMock, membershipServiceMock));
        resourceConfig.register(JacksonFeature.class);
        resourceConfig.register(GenericExceptionMapper.class);
        resourceConfig.register(ValidationExceptionMapper.class);
        resourceConfig.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(userServiceMock).to(UserService.class).in(RequestScoped.class);
            }
        });
        return resourceConfig;
    }

    @Test
    public void shouldReturnSavedUserWhenInvokesUserControllerAddMethod() {
        String login = "user";
        String password = "password";
        UserDto userDto = new UserDto();
        userDto.setLogin(login);
        userDto.setPassword(password);
        UserDto returnedDto = new UserDto();
        returnedDto.setLogin(login);
        returnedDto.setPassword(password);

        when(userServiceMock.save(userDto)).thenReturn(returnedDto);

        Response response =  target("users").request().post(Entity.entity(userDto, MediaType.APPLICATION_JSON));

        assertEquals("Http Response should be 201: ", Response.Status.CREATED.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));

        UserDto responceUser = response.readEntity(UserDto.class);

        assertNotNull(responceUser);
        assertEquals("User login:", login, responceUser.getLogin());
        assertEquals("User password: ", password, responceUser.getPassword());
    }


}
