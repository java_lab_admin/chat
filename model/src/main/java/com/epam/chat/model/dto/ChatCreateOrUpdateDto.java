package com.epam.chat.model.dto;

import com.epam.chat.model.enums.ChatType;
import com.epam.chat.model.validation.EnumValidator;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ChatCreateOrUpdateDto {

    @NotNull(message = "Name required")
    @Size(min = 1, max = 20, message = "Name must be between 1 and 20 characters")
    private String name;

    @EnumValidator(enumClass = ChatType.class, message = "Wrong chat type")
    private String chatType;
}