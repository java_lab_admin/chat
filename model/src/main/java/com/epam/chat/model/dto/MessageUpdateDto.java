package com.epam.chat.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class MessageUpdateDto {

    @NotNull(message = "Text required")
    @Size(min = 1, max = 100, message = "Text must be between 1 and 100 characters")
    private String text;

}
