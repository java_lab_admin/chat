package com.epam.chat.model.dto.response;

import com.epam.chat.model.enums.ChatType;
import lombok.Data;

@Data
public class ChatDto {

    private Long id;

    private String name;

    private ChatType type;
}
