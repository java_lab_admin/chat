package com.epam.chat.model.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class EnumValidatorImpl implements ConstraintValidator<EnumValidator, String> {

    List<String> valueList;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (!valueList.contains(value)) {
            return false;
        }
        return true;
    }

    @Override
    public void initialize(EnumValidator constraintAnnotation) {
        valueList = new ArrayList<String>();
        Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClass();

        @SuppressWarnings("rawtypes")
        Enum[] enumValArray = enumClass.getEnumConstants();

        for (@SuppressWarnings("rawtypes")
            Enum enumVal : enumValArray) {
            valueList.add(enumVal.toString().toUpperCase());
        }
    }
}
