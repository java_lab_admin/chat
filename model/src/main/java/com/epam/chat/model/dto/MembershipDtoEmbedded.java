package com.epam.chat.model.dto;

import com.epam.chat.model.enums.UserRole;
import com.epam.chat.model.validation.EnumValidator;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MembershipDtoEmbedded {
    @NotNull
    @Valid
    private UserLoginDtoEmbedded user;

    @NotEmpty
    @EnumValidator(enumClass = UserRole.class, message = "Wrong user role")
    private String userRole;
}
