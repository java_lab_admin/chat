package com.epam.chat.model.enums;

public enum UserRole {
    ADMIN, USER
}
