package com.epam.chat.model.enums;

public enum ChatType {
    PUBLIC, GROUP
}
