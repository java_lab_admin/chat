package com.epam.chat.model.entity;

import com.epam.chat.model.enums.UserRole;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Membership implements BaseEntity {

    @Id
    @ManyToOne
    private Chat chat;

    @Id
    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private UserRole userRole;
}
