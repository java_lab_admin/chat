package com.epam.chat.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.security.Principal;

@Data
@Entity
@Table(name = "\"USER\"")
public class User implements BaseEntity, Principal {

    @Id
    private String login;

    private String password;

    @Override
    public String getName() {
        return login;
    }
}

