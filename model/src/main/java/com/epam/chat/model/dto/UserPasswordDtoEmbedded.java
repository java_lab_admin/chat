package com.epam.chat.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserPasswordDtoEmbedded {

    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 30, message = "Password must contain from 8 and 30 symbols")
    private String password;
}
