package com.epam.chat.model.dto.response;

import com.epam.chat.model.dto.UserLoginDtoEmbedded;
import com.epam.chat.model.util.LocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MessageDto {

    private Long id;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime creation;

    private String text;

    private UserLoginDtoEmbedded user;

    private ChatDto chat;

}
