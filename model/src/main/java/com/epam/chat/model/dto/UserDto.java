package com.epam.chat.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserDto {

    @NotNull(message = "Login required")
    @Size(min = 1, max = 30, message = "Login must be between 1 and 30")
    private String login;

    @NotNull(message = "Password required")
    @Size(min = 3, max = 15, message = "Password must be between 3 and 15")
    private String password;

}
