package com.epam.chat.model.dto.response;

import com.epam.chat.model.dto.UserLoginDtoEmbedded;
import com.epam.chat.model.enums.UserRole;
import lombok.Data;

@Data
public class MembershipDto {

    private UserLoginDtoEmbedded user;

    private ChatDto chat;

    private UserRole userRole;
}
