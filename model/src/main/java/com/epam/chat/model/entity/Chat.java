package com.epam.chat.model.entity;

import com.epam.chat.model.enums.ChatType;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table
@Data
public class Chat implements BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Enumerated(EnumType.STRING)
    private ChatType type;
}
